package threads;

import javafx.util.Pair;

public class NewThread extends Thread {
	Pair <Integer,Integer> pair;
	int result;
	
	public NewThread(String threadName, Pair <Integer,Integer> newPair) {
		super(threadName);
		this.pair = newPair;
		System.out.println("�������� �����: " + this);
		start();
	}
	public void run() {
		System.out.println("��������: " + this.pair.getKey() +", " + this.pair.getValue());
		result = this.countValue();
		System.out.println("���������: " + result);
		System.out.println(super.getName()+" ��������.");
	}
	public NewThread() {
		
	}
	public int countValue() {
		int a = pair.getKey();
		int b = pair.getValue();
		return a*b;
	}
	public int getResult() {
		return result;
	}
}
