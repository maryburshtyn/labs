package ui;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.layout.GridLayout;
import javafx.util.*;
import lab4V4.ParseVector;
import threads.NewThread;
public class Window {

	 private Display display;
	 private Shell shell;

	 public Window() {

	        display = new Display();
	        shell = new Shell(display, SWT.CLOSE | SWT.TITLE);
	        shell.setText("��������� ��������");


	        createContent();

	        shell.pack();
	        shell.open();

	        while (!shell.isDisposed()) {
	            if (!display.readAndDispatch()) {
	                display.sleep();
	            }
	        }

	        display.dispose();

	    }
	 private void createContent() {
		 	
	      	GridLayout gridLayout = new GridLayout();
	      	gridLayout.numColumns = 3;
	      	shell.setLayout(gridLayout);
	      	
	      	Label vectorALabel = new Label(shell, SWT.LEFT); 
	      	vectorALabel.setText("������ �");
	      	
	      	Label Label = new Label(shell, SWT.LEFT); 
	      	Label.setText("          ");
	      	{
	      		GridData gridData = new GridData(GridData.FILL, GridData.CENTER,false,true,1,1);
	      		Label.setLayoutData(gridData);
	      	}
	      	
	      	Label vectorBLabel = new Label(shell, SWT.BORDER); 
	      	vectorBLabel.setText("������ B");
	      	{
	      		GridData gridData = new GridData(GridData.FILL, GridData.CENTER,false,true,1,1);
	      		vectorBLabel.setLayoutData(gridData);
	      	}
	      	
	      	Text vectorAText = new Text(shell, SWT.NONE);
	      	//vectorAText.setSize(100, 20);
	      	
	      	{
	      		GridData gridData = new GridData(GridData.BEGINNING, GridData.CENTER,false,false,1,1);
	      		vectorAText.setLayoutData(gridData);
	      	}
	      	Label Label1 = new Label(shell, SWT.LEFT); 
	      	Label1.setText("          ");
	      	{
	      		GridData gridData = new GridData(GridData.FILL, GridData.CENTER,false,true,1,1);
	      		Label1.setLayoutData(gridData);
	      	}
	      	Text vectorBText = new Text(shell, SWT.BORDER);
	      	{
	      		GridData gridData = new GridData(GridData.FILL, GridData.CENTER,false,false,1,1);
	      		vectorBText.setLayoutData(gridData);
	      	}
	      	Label multiplyLabel = new Label(shell, SWT.NONE); 
	      	multiplyLabel.setText("������������");
	      	{
	      		GridData gridData = new GridData(GridData.BEGINNING, GridData.CENTER,false,false,1,1);
	      		vectorAText.setLayoutData(gridData);
	      	}
	      	Label Label2 = new Label(shell, SWT.LEFT); 
	      	Label2.setText("          ");
	      	{
	      		GridData gridData = new GridData(GridData.FILL, GridData.CENTER,false,true,2,1);
	      		Label2.setLayoutData(gridData);
	      	}
	      	Text resultText = new Text(shell, SWT.NONE);
	      	{
	      		GridData gridData = new GridData(GridData.BEGINNING, GridData.CENTER,false,false,1,1);
	      		resultText.setLayoutData(gridData);
	      	}
	      	Label Label3 = new Label(shell, SWT.LEFT); 
	      	Label3.setText("          ");
	      	{
	      		GridData gridData = new GridData(GridData.FILL, GridData.CENTER,false,true,1,1);
	      		Label3.setLayoutData(gridData);
	      	}
	      	Button countButton = new Button(shell,SWT.BORDER);
	      	{
	      		GridData gridData = new GridData(GridData.FILL, GridData.CENTER,false,false,1,1);
	      		countButton.setLayoutData(gridData);
	      	}
	      	countButton.setText("���������");
	      	Label errorLabel = new Label(shell, SWT.LEFT); 
	      	errorLabel.setText("          ");
	      	{
	      		GridData gridData = new GridData(GridData.FILL, GridData.CENTER,false,true,3,1);
	      		errorLabel.setLayoutData(gridData);
	      	}
	      	Listener listener = new Listener() {
	            @Override
	            public void handleEvent(Event event) {
	                if(event.widget == countButton) {
	                	errorLabel.setText(" ");
	                	ParseVector vecA,vecB;
	                	ArrayList<Integer> vectorA,vectorB;
	                	vectorA = new ArrayList<>();
	                	vectorB = new ArrayList<>();
	                	vecA = new ParseVector(vectorAText.getText());
	                	vecB = new ParseVector(vectorBText.getText());
	                	vecA.parse();
	                	vecB.parse();
	                	if(vecA.parseError() || vecB.parseError()) {
	                		errorLabel.setText("�������� ����");
	                	}
	                	else {
	                		vectorA = vecA.parse();
	                		vectorB = vecB.parse();
	                	if(vectorA.size()!=vectorB.size()) {
	                		errorLabel.setText("������� ������ ���� ���������� �����");
	                	}
	                	else {
	                	Integer sum = 0;
	            		ArrayList<NewThread> threadList = new ArrayList<>();
	            		int mul = 0;
	            		NewThread thread = new NewThread();
	            		for (int i = 0; i < vectorA.size(); i++) {
	            		Pair <Integer,Integer> newPair = new Pair<>(vectorA.get(i),vectorB.get(i));
	            		
	            		thread = new NewThread("Thread"+i,newPair); 
	            		threadList.add(thread);
	            		
	            		}
	            		for (int i = 0; i < vectorA.size(); i++) {
	            			
	            		try { 
	            			threadList.get(i).join();
	            		} catch (InterruptedException e) {
	            			System.out.println("����� " + threadList.get(i).getName() + "�� ��������");
	            		}
	            		mul = threadList.get(i).getResult();
	            		sum += mul;
	            		}
	            		
	            		System.out.println("�����: " + sum);	
	            		resultText.setText(sum.toString());
	                	}
	                	}
	                }
	                	

	            }
	        };

	        countButton.addListener(SWT.Selection, listener);
	    }
}
