package lab4V4;

import java.util.ArrayList;

public class ParseVector {
	String vectorString;
	boolean error;
	public ParseVector(String vect) {
		this.vectorString = vect;
	}
	public boolean parseError() {
		return error;
	}
	public ArrayList<Integer> parse(){
		ArrayList<Integer> vector = new ArrayList<>();
		String str = new String();
		for(int i = 0; i < vectorString.length(); i ++) {
			if(vectorString.charAt(0)!= '{' || vectorString.charAt(vectorString.length()-1)!= '}') {
				error = true;
				return vector;
			}
			if(vectorString.charAt(i) == '{') {
				continue;
			}
			if(vectorString.charAt(i) != ',' && vectorString.charAt(i) != '}') {
				if(vectorString.charAt(i-1) == ',' || vectorString.charAt(i-1) == '{') {
					str = new String(Character.toString(vectorString.charAt(i)));
				}
				else {
					str += Character.toString(vectorString.charAt(i));
				}
			}
			if(vectorString.charAt(i) == ',' || vectorString.charAt(i) == '}') {
				vector.add(Integer.parseInt(str));
			}
		}
		return vector;
	}
}
